import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CustomerBlend } from '@models/customer-blend';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlendingService {
  private url = 'blends';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'my-auth-token'
      })
    };
  }

  public getCustomerBlendsByCropAndType(crop: number, type: string): Observable<CustomerBlend[]> {
    return this.http.get<CustomerBlend[]>(this.url + '?crop=' + crop + '&type=' + type);
  }

  public getCustomerBlends(crop: number, type: string) {
    return this.http.get<CustomerBlend[]>(this.url + '?crop=' + crop + '&type=' + type);
  }

  public getCustomerBlendsAsync(crop: number, type: string) {
    return this.http.get(this.url + '?crop=' + crop + '&type=' + type)
      .pipe(map((response: Response) => response.json()));
  }

  public save(customerBlends: CustomerBlend[]) {
    return this.http.post<CustomerBlend[]>(this.url, customerBlends, this.httpOptions);
  }
}
