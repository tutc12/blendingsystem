import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TobaccoType } from '@models/tobacco-type';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypeService {

  private url = 'tobaccotypes';

  constructor(private http: HttpClient) {
  }

  public getTypes(): Observable<TobaccoType[]> {
    return this.http.get<TobaccoType[]>(this.url);
  }
}
