import { Injectable } from '@angular/core';
import { CustomerSpec } from '@models/customer-spec';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerSpecService {

  private url = 'customerspecs';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getCustomerSpecByCropAndType(crop: number, type: string): Observable<CustomerSpec[]> {
    return this.http.get<CustomerSpec[]>(this.url + '?crop=' + crop + '&type=' + type);
  }

  public save(cusSpecs: CustomerSpec[]) {
    return this.http.put<CustomerSpec[]>(this.url, cusSpecs, this.httpOptions);
  }

  public add(cusSpec: CustomerSpec) {
    return this.http.post<CustomerSpec>(this.url, cusSpec, this.httpOptions);
  }

  public edit(cusSpecs: CustomerSpec) {
    return this.http.put<CustomerSpec>(this.url + '/edit/' + cusSpecs.id, cusSpecs, this.httpOptions);
  }

  public delete(cusSpec: CustomerSpec) {
    return this.http.delete(this.url + '?id=' + cusSpec.id, this.httpOptions);
  }
}
