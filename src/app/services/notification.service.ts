import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }

  showSuccess(message: string, title: string) {
    this.toastr.success(message, title, { positionClass: 'toast-bottom-full-width', closeButton: true });
  }

  showWarning(message: string, title: string) {
    this.toastr.warning(message, title, { positionClass: 'toast-bottom-full-width', closeButton: true });
  }

  showError(message: string, title: string) {
    this.toastr.error(message, title, { positionClass: 'toast-bottom-full-width', closeButton: true });
  }

  showInfo(message: string, title: string) {
    this.toastr.info(message, title, { positionClass: 'toast-bottom-full-width', closeButton: true });
  }
}
