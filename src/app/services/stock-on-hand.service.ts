import { StockOnHand } from './../models/stock-on-hand';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StockOnHandService {

  private url = 'stockonhands';

  constructor(private http: HttpClient) { }

  public getStockOnHands(crop: number, type: string): Observable<StockOnHand[]> {
    return this.http.get<StockOnHand[]>(this.url + '?crop=' + crop + '&type=' + type);
  }
}
