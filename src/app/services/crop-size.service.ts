import { CropSize } from '@models/crop-size';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CropSizeService {
  private url = 'cropsizes';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getAll() {
    return this.http.get<CropSize[]>(this.url);
  }

  public getCropSize(crop: number, type: string) {
    return this.http.get<CropSize>(this.url + '?crop=' + crop + '&type=' + type);
  }

  public add(item: CropSize) {
    return this.http.post(this.url, item, this.httpOptions);
  }

  public edit(cropSize: CropSize) {
    return this.http.put<CropSize>(this.url + '?id=' + cropSize.id, JSON.stringify(cropSize), this.httpOptions);
  }

  public delete(id: string) {
    return this.http.delete(this.url + '?id=' + id);
  }
}
