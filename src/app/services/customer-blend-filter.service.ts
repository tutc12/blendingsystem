import { CustomerBlendFilter } from '@models/customer-blend-filter';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { CustomerBlend } from '@models/customer-blend';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerBlendFilterService {
  private url = 'customerblendfilters';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getByCropAndType(crop: number, type: string) {
    return this.http.get<CustomerBlendFilter[]>(this.url + '?crop=' + crop + '&type=' + type);
  }

  public add(cusBlend: CustomerBlend) {
    return this.http.post(this.url, cusBlend, this.httpOptions);
  }

  public delete(id: string) {
    return this.http.delete(this.url + '?id=' + id);
  }
}
