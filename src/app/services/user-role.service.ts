import { UserRole } from './../models/user-role';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserRoleService {

  private url = 'userroles';

  constructor(private http: HttpClient) { }

  public getUserRoles(username: string): Observable<UserRole[]> {
    return this.http.get<UserRole[]>(this.url + '?userName=' + username);
  }
}
