import { Observable } from 'rxjs';
import { PackedCaseNetKg } from '@models/packed-case-net-kg';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PackedCasesNetKgService {

  private url = 'packedstocknetkgs';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<PackedCaseNetKg[]> {
    return this.http.get<PackedCaseNetKg[]>(this.url);
  }

  public add(packedCasesNetKg: PackedCaseNetKg) {
    return this.http.post<PackedCaseNetKg>(this.url, packedCasesNetKg, this.httpOptions);
  }

  public edit(packedCasesNetKg: PackedCaseNetKg) {
    return this.http.put<PackedCaseNetKg>(this.url + '?id=' + packedCasesNetKg.id, packedCasesNetKg, this.httpOptions);
  }

  public delete(id: string) {
    return this.http.delete<PackedCaseNetKg>(this.url + '?id=' + id, this.httpOptions);
  }
}
