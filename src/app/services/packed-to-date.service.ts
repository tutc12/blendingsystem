import { PackedToDate } from '@models/packed-to-date';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PackedToDateService {
  private url = 'PackedToDates';
  constructor(private http: HttpClient) {
  }

  public getPackedToDateByCropAndType(crop: number, type: string): Observable<PackedToDate[]> {
    return this.http.get<PackedToDate[]>(this.url + '?crop=' + crop + '&type=' + type);
  }
}
