import { CustomerSpecFilter } from '@models/customer-spec-filter';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CustomerSpec } from '@models/customer-spec';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerSpecFilterService {

  private url = 'customerspecfilters';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getByCropAndType(crop: number, type: string) {
    // return CustomerSpecFilterMockup.filter((item) => item.crop === crop && item.type === type);
    return this.http.get<CustomerSpecFilter[]>(this.url + '?crop=' + crop + '&type=' + type);
  }

  public add(cusSpec: CustomerSpec) {
    return this.http.post(this.url, cusSpec, this.httpOptions);
  }

  public delete(id: string) {
    return this.http.delete(this.url + '?id=' + id);
  }
}
