import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Crop } from '@models/crop';

@Injectable({
  providedIn: 'root'
})
export class CropService {

  private url = 'crops';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getCrops(): Observable<Crop[]> {
    return this.http.get<Crop[]>(this.url);
  }

  public getDefaultCrop(): Observable<Crop> {
    return this.http.get<Crop>(this.url + '/getDefaultCrop');
  }
}
