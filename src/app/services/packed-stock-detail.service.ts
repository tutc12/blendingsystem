import { PackedStockDetail } from '@models/packed-stock-detail';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PackedStockDetailService {

  private url = 'PackedStockDetails';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getByCustomerSpec(id: string): Observable<PackedStockDetail[]> {
    return this.http.get<PackedStockDetail[]>(this.url + '?cusSpecId=' + id);
  }

  public add(packedStockDetail: PackedStockDetail) {
    return this.http.post(this.url, packedStockDetail, this.httpOptions);
  }

  public edit(packedStockDetail: PackedStockDetail) {
    return this.http.put(this.url + '?id=' + packedStockDetail.id, packedStockDetail, this.httpOptions);
  }

  public delete(id: string) {
    return this.http.delete(this.url + '?id=' + id);
  }
}
