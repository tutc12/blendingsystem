import { StockOnHand } from '@models/stock-on-hand';
import { StockOnHandService } from '@services/stock-on-hand.service';
import { Component, OnInit } from '@angular/core';
import { Crop } from '@models/crop';
import { TobaccoType } from '@models/tobacco-type';
import { CropService } from '@services/crop.service';
import { TypeService } from '@services/type.service';

@Component({
  selector: 'app-report04',
  templateUrl: './report04.component.html',
  styleUrls: ['./report04.component.css']
})
export class Report04Component implements OnInit {

  constructor(private stockOnHandService: StockOnHandService,
    private cropService: CropService,
    private typeService: TypeService) { }

  public chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public chartLabel: Array<any> = [];
  public chartData: Array<any> = [{ data: [] }];
  public chartType = 'bar';
  public chartLegend = true;

  public crops: Crop[];
  public types: TobaccoType[];
  public stockOnHands: StockOnHand[];
  public selectedCrop: number;
  public selectedType: string;

  async ngOnInit() {
    const crop = await this.cropService.getDefaultCrop().toPromise();
    this.crops = await this.cropService.getCrops().toPromise();
    this.types = await this.typeService.getTypes().toPromise();
    this.selectedCrop = crop.crop;
    this.selectedType = 'BU';
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onCropChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onTypeChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  async bindingChart(crop: number, type: string) {
    const info: Array<any> = [];
    const label: Array<any> = [];

    this.stockOnHands = await this.stockOnHandService.getStockOnHands(crop, type).toPromise();
    this.stockOnHands.forEach(res => {
      info.push(res.weight);
      label.push(res.classify);
    });

    this.chartData = [{ data: info, label: 'Stock' }];
    this.chartLabel = label;
  }

}
