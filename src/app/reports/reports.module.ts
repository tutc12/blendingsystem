import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { Report01Component } from './report01/report01.component';
import { Report02Component } from './report02/report02.component';
import { FormsModule } from '@angular/forms';
import { ClarityModule, ClrFormsNextModule } from '@clr/angular';
import { ChartsModule } from 'ng2-charts';
import { Report03Component } from './report03/report03.component';
import { MainComponent } from './main/main.component';
import { Report04Component } from './report04/report04.component';
import { Report05Component } from './report05/report05.component';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    FormsModule,
    ClarityModule,
    ClrFormsNextModule,
    ChartsModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  declarations: [LayoutComponent,
    PageNotFoundComponent,
    Report01Component,
    Report02Component,
    Report03Component,
    MainComponent,
    Report04Component,
    Report05Component]
})
export class ReportsModule { }
