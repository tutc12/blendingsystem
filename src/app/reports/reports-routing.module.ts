
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from 'app/auth/role.guard';
import { LayoutComponent } from './layout/layout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MainComponent } from './main/main.component';
import { Report01Component } from './report01/report01.component';
import { Report02Component } from './report02/report02.component';
import { Report03Component } from './report03/report03.component';
import { Report04Component } from './report04/report04.component';
import { Report05Component } from './report05/report05.component';

const routes: Routes = [
  {
    path: 'reports',
    component: LayoutComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRole: ['Admin', 'User']
    },
    children: [
      { path: '', redirectTo: 'main', pathMatch: 'full' },
      { path: 'main', component: MainComponent },
      { path: 'report01', component: Report01Component },
      { path: 'report02', component: Report02Component },
      { path: 'report03', component: Report03Component },
      { path: 'report04', component: Report04Component },
      { path: 'report05', component: Report05Component },
      { path: '**', component: PageNotFoundComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
