import { CustomerSpec } from './../../models/customer-spec';
import { CustomerSpecService } from '@services/customer-spec.service';
import { Component, OnInit } from '@angular/core';
import { CropService } from '@services/crop.service';
import { TypeService } from '@services/type.service';
import { Crop } from '@models/crop';
import { TobaccoType } from '@models/tobacco-type';

@Component({
  selector: 'app-report05',
  templateUrl: './report05.component.html',
  styleUrls: ['./report05.component.css']
})
export class Report05Component implements OnInit {

  constructor(private customerSpecService: CustomerSpecService,
    private cropService: CropService,
    private typeService: TypeService) { }

  public chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public chartLabel: Array<any> = [];
  public chartData: Array<any> = [{ data: [] }];
  public chartType = 'doughnut';
  public chartLegend = true;

  public crops: Crop[];
  public types: TobaccoType[];
  public cusSpecs: CustomerSpec[];
  public selectedCrop: number;
  public selectedType: string;

  async ngOnInit() {
    const crop = await this.cropService.getDefaultCrop().toPromise();
    this.crops = await this.cropService.getCrops().toPromise();
    this.types = await this.typeService.getTypes().toPromise();
    this.selectedCrop = crop.crop;
    this.selectedType = 'BU';
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onCropChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onTypeChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  async bindingChart(crop: number, type: string) {
    const info: Array<any> = [];
    const label: Array<any> = [];

    this.cusSpecs = await this.customerSpecService.getCustomerSpecByCropAndType(crop, type).toPromise();

    const orders = this.cusSpecs.reduce((sum, value) => sum += value.orderCase, 0);
    const packedCases = this.cusSpecs.reduce((sum, value) => sum += value.packedCases, 0);
    info.push(Math.round((orders - packedCases) / orders * 100));
    info.push(Math.round(packedCases / orders * 100));
    label.push('wait for packed');
    label.push('packed');

    this.chartData = [{ data: info }];
    this.chartLabel = label;
  }

}
