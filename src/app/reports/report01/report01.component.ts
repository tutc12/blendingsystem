import { Component, OnInit } from '@angular/core';
import { CustomerSpecService } from '@services/customer-spec.service';
import { CustomerSpec } from '@models/customer-spec';
import { Crop } from '@models/crop';
import { TobaccoType } from '@models/tobacco-type';
import { CropService } from '@services/crop.service';
import { TypeService } from '@services/type.service';

@Component({
  selector: 'app-report01',
  templateUrl: './report01.component.html',
  styleUrls: []
})
export class Report01Component implements OnInit {

  // constructor() { }

  // public barChartOptions = {
  //   scaleShowVerticalLines: false,
  //   responsive: true
  // };
  // public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  // public barChartType = 'bar';
  // public barChartLegend = true;
  // public barChartData = [
  //   { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  //   { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  // ];

  // ngOnInit() {
  // }
  constructor(private customerSpecService: CustomerSpecService,
    private cropService: CropService,
    private typeService: TypeService) { }

  public chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public chartLabel: Array<any> = [];
  public chartData: Array<any> = [{ data: [] }];
  public chartType = 'bar';
  public chartLegend = true;
  public customerSpecs: CustomerSpec[];
  public crops: Crop[];
  public types: TobaccoType[];
  public selectedCrop: number;
  public selectedType: string;

  async ngOnInit() {
    const crop = await this.cropService.getDefaultCrop().toPromise();
    this.crops = await this.cropService.getCrops().toPromise();
    this.types = await this.typeService.getTypes().toPromise();
    this.selectedCrop = crop.crop;
    this.selectedType = 'BU';
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onCropChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onTypeChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  async bindingChart(crop: number, type: string) {
    const info1: Array<any> = [];
    const info2: Array<any> = [];
    const label: Array<any> = [];
    this.customerSpecs = await this.customerSpecService.getCustomerSpecByCropAndType(crop, type).toPromise();
    this.customerSpecs.forEach(cusSpec => {
      info1.push(cusSpec.orderCase);
      info2.push(cusSpec.packedCases);
      label.push(cusSpec.packedGrade);
    });

    this.chartData = [
      { data: info1, label: 'Order' },
      { data: info2, label: 'Packed' }
    ];
    this.chartLabel = label;
  }

}
