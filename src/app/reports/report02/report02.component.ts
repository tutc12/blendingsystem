import { Component, OnInit } from '@angular/core';
import { CustomerSpecService } from '@services/customer-spec.service';
import { CustomerSpec } from '@models/customer-spec';
import { Crop } from '@models/crop';
import { TobaccoType } from '@models/tobacco-type';
import { CropService } from '@services/crop.service';
import { TypeService } from '@services/type.service';

@Component({
  selector: 'app-report02',
  templateUrl: './report02.component.html',
  styleUrls: []
})
export class Report02Component implements OnInit {

  constructor(private customerSpecService: CustomerSpecService,
    private cropService: CropService,
    private typeService: TypeService) { }

  public chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public chartLabel: Array<any> = [];
  public chartData: Array<any> = [{ data: [] }];
  public chartType = 'bar';
  public chartLegend = true;
  public customerSpecs: CustomerSpec[];
  public crops: Crop[];
  public types: TobaccoType[];
  public selectedCrop: number;
  public selectedType: string;

  async ngOnInit() {
    const crop = await this.cropService.getDefaultCrop().toPromise();
    this.crops = await this.cropService.getCrops().toPromise();
    this.types = await this.typeService.getTypes().toPromise();
    this.selectedCrop = crop.crop;
    this.selectedType = 'BU';
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onCropChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }

  onTypeChange() {
    this.bindingChart(this.selectedCrop, this.selectedType);
  }
  async bindingChart(crop: number, type: string) {
    const info: Array<any> = [];
    const label: Array<any> = [];

    this.customerSpecs = await this.customerSpecService.getCustomerSpecByCropAndType(crop, type).toPromise();

    this.customerSpecs.map(x => {
      if (label.indexOf(x.customer) <= -1) {
        label.push(x.customer);
        info.push(this.customerSpecs.filter(y => y.customer === x.customer)
          .reduce((sum, value) => sum += value.orderCase, 0));
      }
    });

    this.chartData = [{ data: info, label: 'Order' }];
    this.chartLabel = label;
  }

}
