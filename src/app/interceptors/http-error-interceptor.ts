import { NotificationService } from './../services/notification.service';
import { Injectable, Injector } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(private notificationService: NotificationService) {

    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                retry(1),
                catchError((error: HttpErrorResponse) => {
                    const errorResponse: any = '';
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        this.notificationService.showError(error.error.message, 'Client error!');
                    } else {
                        // server-side error
                        if (error.status === 400) {
                            if (error.error.error != null) {
                                this.notificationService
                                    .showError(error.error.error, 'Server error 400!');
                            } else {
                                this.notificationService
                                    .showError(error.error.message, 'Server error 400!');
                            }
                            // cannot return unauthorized 401 from token system (Authen api project)
                        } else if (error.status === 401) {
                            this.notificationService
                                .showError('Unauthorized to access for this resource.' +
                                    'Description: ' + error.error.message, 'Server error 401!');
                        } else if (error.status === 404) {
                            this.notificationService
                                .showError('The resource not found. ' +
                                    'Please send this problem to IT dept for fix this issue.' +
                                    'Description: ' + error.error.message, 'Server error 404!');
                        } else if (error.status === 500) {
                            this.notificationService
                                .showError('Internal server error. ' +
                                    'Please send this problem to IT dept for fix this issue. ' +
                                    'Description: ' + error.error.message, 'Server error 500!');
                        } else {
                            this.notificationService
                                .showError('error code ' + error.status + '\n' +
                                    'The resource server not response. Please contect IT dept', 'Server error!');
                        }
                    }
                    return throwError(errorResponse);
                })
            );
    }
}
