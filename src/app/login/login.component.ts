import { UserRole } from './../models/user-role';
import { UserRoleService } from './../services/user-role.service';
import { AuthService } from './../auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '@services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  returnUrl: string;
  userRoles: UserRole[];

  constructor(private authService: AuthService,
    private userRoleService: UserRoleService,
    private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit() {
    localStorage.removeItem('access-token');
    localStorage.removeItem('user-roles');
  }

  async login() {
    if (this.username == null || this.username === '') {
      alert('Please input username!');
      return;
    }
    if (this.password == null || this.password === '') {
      alert('Please input password!');
      return;
    }

    localStorage.removeItem('access-token');
    localStorage.removeItem('user-roles');

    const token = await this.authService.login(this.username, this.password);
    if (token == null) {
      this.notificationService.showWarning('You dont have a permission to this site. Please contact administrator.', 'warning!');
    } else {
      const userRole = await this.userRoleService.getUserRoles(this.username).toPromise();
      if (userRole != null) {
        localStorage.setItem('user-roles', JSON.stringify(userRole));
        this.router.navigate(['user/packed-to-date']);
        this.notificationService.showSuccess('Login successfull', 'complete');
      }
    }
  }
}
