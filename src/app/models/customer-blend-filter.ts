export class CustomerBlendFilter {
    crop: number;
    type: string;
    classify: string;
    cropSizeDetailId: string;

    constructor(crop: number,
        type: string,
        classify: string,
        cropSizeDetailId: string) {
        this.crop = crop;
        this.type = type;
        this.classify = classify;
        this.cropSizeDetailId = cropSizeDetailId;
    }
}
