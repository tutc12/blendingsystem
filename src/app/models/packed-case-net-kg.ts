export class PackedCaseNetKg {
    id: string;
    netKg: number;
    createDate: Date;
    createBy: string;
    modifiedDate: Date;
    modifiedBy: string;

    constructor(
        id: string,
        netKg: number,
        createDate: Date,
        createBy: string,
        modifiedDate: Date,
        modifiedBy: string,
    ) {
        this.id = id;
        this.netKg = netKg;
        this.createDate = createDate;
        this.createBy = createBy;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
    }
}
