export class PackedGrade {
    packedgrade: string;
    crop: number;
    type: string;
    customer: string;
    form: string;
    netdef: number;
    taredef: number;
    grossdef: number;
    gepbaht: number;
    packingmat: string;
    dtrecord: Date;
    pduser: string;
    redrycode: string;
    transportcode: string;

    constructor(packedgrade: string,
        crop: number,
        type: string,
        customer: string,
        form: string,
        netdef: number,
        taredef: number,
        grossdef: number,
        gepbaht: number,
        packingmat: string,
        dtrecord: Date,
        pduser: string,
        redrycode: string,
        transportcode: string) {
        this.packedgrade = packedgrade;
        this.crop = crop;
        this.type = type;
        this.customer = customer;
        this.form = form;
        this.netdef = netdef;
        this.taredef = taredef;
        this.grossdef = grossdef;
        this.gepbaht = gepbaht;
        this.packingmat = packingmat;
        this.dtrecord = dtrecord;
        this.pduser = pduser;
        this.redrycode = redrycode;
        this.transportcode = transportcode;
    }

}
