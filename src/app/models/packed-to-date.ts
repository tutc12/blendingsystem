export class PackedToDate {
    customer: string;
    crop: number;
    type: string;
    packedGrade: string;
    netKg: number;
    orderCase: number;
    packedCases: number;
    actualGreen: number;
    actualYield: number;
    manualYield: number;
    packedStockKg: number;
    packedStockCases: number;
    casePerHour: number;

    constructor(customer: string,
        crop: number,
        type: string,
        packedGrade: string,
        netKg: number,
        packedCases: number,
        actualGreen: number,
        orderCase: number,
        actualYield: number,
        manualYield: number,
        packedStockKg: number,
        packedStockCases: number,
        casePerHour: number) {
        this.customer = customer;
        this.crop = crop;
        this.type = type;
        this.packedGrade = packedGrade;
        this.netKg = netKg;
        this.orderCase = orderCase;
        this.packedCases = packedCases;
        this.actualGreen = actualGreen;
        this.actualYield = actualYield;
        this.manualYield = manualYield;
        this.packedStockKg = packedStockKg;
        this.packedStockCases = packedStockCases;
        this.casePerHour = casePerHour;
    }
}
