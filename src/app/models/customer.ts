export class Customer {
    code: string;
    name: string;
    // address: string;
    // tel: string;
    // fax: string;
    // url: string;
    // email: string;
    // remark: string;
    // dtrecord: Date;
    // user: string;
    // UseOrders: boolean;

    constructor(code: string,
        name: string) {
        this.code = code;
        this.name = name;
    }
}
