export class Crop {
    crop: number;
    default: boolean;

    constructor(crop: number, def: boolean) {
        this.crop = crop;
        this.default = def;
    }
}
