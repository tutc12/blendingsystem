import { Blend } from './blend';

export class CustomerBlend {
    crop: number;
    type: string;
    classify: string;
    cropThrow: number;
    stock: number;
    actualBought: number;
    accumulativeRemainningBlending: number; // use for blending plan.
    cropSizeDetailId: string;
    blends: Blend[];
    show: Boolean;

    constructor(crop: number,
        type: string,
        classify: string,
        cropThrow: number,
        stock: number,
        actualBought: number,
        accumulativeRemainningBlending: number,
        cropSizeDetailId: string,
        blends: Blend[]) {
        this.crop = crop;
        this.type = type;
        this.classify = classify;
        this.cropThrow = cropThrow;
        this.stock = stock;
        this.actualBought = actualBought;
        this.accumulativeRemainningBlending = accumulativeRemainningBlending;
        this.cropSizeDetailId = cropSizeDetailId,
        this.blends = blends;
    }
}
