export class StockOnHand {
    type: string;
    level: string;
    classify: string;
    weight: string;

    constructor(classify: string,
        level: string,
        type: string,
        weight: string) {
        this.classify = classify;
        this.type = type;
        this.weight = weight;
        this.level = level;
    }
}
