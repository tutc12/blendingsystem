export class TobaccoType {
    type: string;
    desc: string;
    dtrecord: Date;
    user: string;

    constructor(type: string,
        desc: string,
        dtrecord: Date,
        user: string) {
        this.type = type;
        this.desc = desc;
        this.dtrecord = dtrecord;
        this.user = user;
    }
}
