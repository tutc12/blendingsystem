export class CropSize {
    id: string;
    crop: number;
    type: string;
    cropSize: number;
    modifiedDate: string;
    modifiedBy: string;

    constructor(id: string,
        crop: number,
        type: string,
        cropSize: number,
        modifiedDate: string,
        modifiedBy: string) {
        this.id = id;
        this.crop = crop;
        this.cropSize = cropSize;
        this.type = type;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
    }
}
