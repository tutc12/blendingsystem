export class CustomerSpec {
    id: string;
    crop: number;
    type: string;
    customer: string;
    packedGrade: string;
    netKg: number;
    orderCase: number;
    packedCases: number; // from calculation.
    actualGreen: number; // from calculation.
    actualYield: number;
    manualYield: number;
    packedStockCases: number;
    packedStockKg: number;
    accumulativeBlenPercent: number; // use for blending plan.
    accumulativeGreenEstimate: number; // use for blending plan.
    show: Boolean;

    constructor(id: string,
        crop: number,
        type: string,
        customer: string,
        packedGrade: string,
        netKg: number,
        orderCase: number,
        packedCases: number,
        actualGreen: number,
        actualYield: number,
        manualYield: number,
        packedStockCases: number,
        packedStockKg: number,
        accumulativeBlenPercent: number,
        accumulativeGreenEstimate: number) {
        this.id = id;
        this.crop = crop;
        this.type = type;
        this.customer = customer;
        this.packedGrade = packedGrade;
        this.netKg = netKg;
        this.orderCase = orderCase;
        this.packedCases = packedCases;
        this.actualGreen = actualGreen;
        this.actualYield = actualYield;
        this.manualYield = manualYield;
        this.packedStockCases = packedStockCases;
        this.packedStockKg = packedStockKg;
        this.accumulativeBlenPercent = accumulativeBlenPercent;
        this.accumulativeGreenEstimate = accumulativeGreenEstimate;
    }
}
