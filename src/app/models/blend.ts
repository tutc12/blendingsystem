export class Blend {
    blendPercent: number;
    green: number;
    customer: string;
    packedGrade: string;
    show: Boolean;

    constructor(blendPercent: number,
        green: number,
        customer: string,
        packedGrade: string,
        show: Boolean) {
        this.blendPercent = blendPercent;
        this.green = green;
        this.customer = customer;
        this.packedGrade = packedGrade;
        this.show = show;
    }
}
