export class UserRole {
    roleName: string;

    constructor(roleName: string) {
        this.roleName = roleName;
    }
}
