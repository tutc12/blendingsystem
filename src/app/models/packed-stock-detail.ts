export class PackedStockDetail {
    id: string;
    custSpecId: string;
    packedCasesNetKgID: string;
    netKg: number;
    packedCases: number;

    constructor(id: string,
        custSpecId: string,
        packedCasesNetKgID: string,
        netKg: number,
        packedCases: number) {
        this.id = id;
        this.custSpecId = custSpecId;
        this.packedCasesNetKgID = packedCasesNetKgID;
        this.netKg = netKg;
        this.packedCases = packedCases;
    }
}
