export class CustomerSpecFilter {
    crop: number;
    type: string;
    customer: string;
    packedGrade: string;
    id: string;
    customerSpecId: string;

    constructor(crop: number,
        type: string,
        customer: string,
        packedGrade: string,
        id: string,
        customerSpecId: string) {
        this.crop = crop;
        this.type = type;
        this.customer = customer;
        this.packedGrade = packedGrade;
        this.id = id;
        this.customerSpecId = customerSpecId;
    }
}
