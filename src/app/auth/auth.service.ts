import { UserRoleService } from './../services/user-role.service';
import { UserRole } from '@models/user-role';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
    private router: Router,
    private userRoleService: UserRoleService) { }

  public isAuthenticated(): Boolean {
    const token = localStorage.getItem('access-token');

    if (!token) {
      return false;
    } else {
      return true;
    }
  }

  public async login(username: string, password: string) {
    const url = 'http://192.168.0.25/Authen/token';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      })
    };

    const token = await this.http.post<any>(url, 'grant_type=password' +
      '&username=' + username +
      '&password=' + password +
      '&client_id=ngLocalHost', httpOptions).toPromise();

    if (token != null) {
      localStorage.setItem('access-token', token.access_token);
      return localStorage.getItem('access-token');
    }
  }

  public logOut() {
    localStorage.removeItem('access-token');
    this.router.navigate(['user/main']);
  }
}
