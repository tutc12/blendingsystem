import { UserRole } from '@models/user-role';
import { UserRoleService } from './../services/user-role.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { NotificationService } from '@services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  _userRole: UserRole[];

  constructor(private userRoleService: UserRoleService,
    private notificationService: NotificationService,
    public router: Router) { }

  async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {

    let expectedRoleArray = route.data;
    expectedRoleArray = expectedRoleArray.expectedRole;
    this._userRole = JSON.parse(localStorage.getItem('user-roles'));

    if (this._userRole === null) {
      this.notificationService.showWarning('You dont have a permission to this site. Please contact administrator.', 'warning!');
      return false;
    } else {
      let result = 0;
      for (let index = 0; index < expectedRoleArray.length; index++) {
        if (this._userRole.filter(x => x.roleName === expectedRoleArray[index]).length > 0) {
          result = result + 1;
        }
      }
      if (result === 0) {
        this.notificationService.showWarning('You dont have a permission to this site. Please contact administrator.', 'warning!');
        return false;
      } else {
        return true;
      }
    }
  }

}
