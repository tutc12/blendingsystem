import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ClarityModule, ClrFormsNextModule } from '@clr/angular';
import { UserManageRoutingModule } from './user-manage-routing.module';

import { LayoutComponent } from './layout/layout.component';
import { BlendingPlanComponent } from './blending-plan/blending-plan.component';
import { PackedToDateComponent } from './packed-to-date/packed-to-date.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CustomerSpecListComponent } from './customer-spec/customer-spec-list/customer-spec-list.component';
import { PackedCaseNetKgComponent } from './packed-case-net-kg/packed-case-net-kg.component';
import { CropSizeComponent } from './crop-size/crop-size.component';

@NgModule({
  imports: [
    CommonModule,
    UserManageRoutingModule,
    FormsModule,
    ClarityModule,
    ClrFormsNextModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    LayoutComponent,
    BlendingPlanComponent,
    PackedToDateComponent,
    PageNotFoundComponent,
    CustomerSpecListComponent,
    PackedCaseNetKgComponent,
    CropSizeComponent
  ],
  providers: []
})
export class UserManageModule { }
