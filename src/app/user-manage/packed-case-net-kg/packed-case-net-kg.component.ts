import { PackedCasesNetKgService } from '@services/packed-cases-net-kg.service';
import { PackedCaseNetKg } from '@models/packed-case-net-kg';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from '@services/notification.service';

@Component({
  selector: 'app-packed-case-net-kg',
  templateUrl: './packed-case-net-kg.component.html',
  styleUrls: []
})
export class PackedCaseNetKgComponent implements OnInit {

  public packedCaseNetKg = new PackedCaseNetKg(null, null, null, null, null, null);
  public packedCasesNetKg: PackedCaseNetKg[];
  public addStatus: Boolean = true;

  constructor(private packedCaseNetKgService: PackedCasesNetKgService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.reloadList();
  }

  public reloadList() {
    this.addStatus = true;
    this.packedCaseNetKgService.getAll().subscribe(res => {
      this.packedCasesNetKg = res.sort(function (obj1, obj2) {
        return obj1.netKg - obj2.netKg;
      });
    });
  }

  public add() {
    this.packedCaseNetKgService.add(this.packedCaseNetKg).subscribe(() => { },
      () => {
        alert('Add data success!');
        this.reloadList();
      });
  }

  public edit() {
    this.packedCaseNetKgService.edit(this.packedCaseNetKg).subscribe(() => { },
      () => {
        alert('Edit data success!');
        this.reloadList();
      });
  }

  public delete(id: string) {
    if (confirm('Are you sure to delete?')) {
      this.packedCaseNetKgService.delete(id).subscribe(() => { },
        () => {
          alert('Delete data success!');
          this.reloadList();
        });
    }
  }

  public onSelected(packedCaseNetKg: PackedCaseNetKg) {
    this.packedCaseNetKg = packedCaseNetKg;
    this.addStatus = false;
  }
}
