import { Component, OnInit } from '@angular/core';
import { TobaccoType } from '@models/tobacco-type';
import { Crop } from '@models/crop';
import { Customer } from '@models/customer';
import { CustomerSpec } from '@models/customer-spec';
import { PackedStockDetail } from '@models/packed-stock-detail';
import { CustomerSpecService } from '@services/customer-spec.service';
import { PackedCasesNetKgService } from '@services/packed-cases-net-kg.service';
import { PackedStockDetailService } from '@services/packed-stock-detail.service';
import { PackedCaseNetKg } from '@models/packed-case-net-kg';
import { CustomerService } from '@services/customer.service';
import { TypeService } from '@services/type.service';
import { CropService } from '@services/crop.service';
import { NotificationService } from '@services/notification.service';

@Component({
  selector: 'app-customer-spec-list',
  templateUrl: './customer-spec-list.component.html',
  styleUrls: ['./customer-spec-list.component.css']
})
export class CustomerSpecListComponent implements OnInit {

  public crops: Crop[];
  public types: TobaccoType[];
  public customers: Customer[];
  public customerSpecs: CustomerSpec[];
  public customerSpec: CustomerSpec = new CustomerSpec(null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null);
  public selectedCrop: number;
  public selectedType: string;
  public showAddOrEditModal: Boolean;
  public showPackedStockModal: Boolean;
  public addOrEditTitle: string;
  public addStatus: Boolean;
  public packedStock: PackedStockDetail = new PackedStockDetail(null, null, null, null, null);
  public packedStockList: PackedStockDetail[];
  public packedCasesNetKgList: PackedCaseNetKg[];
  public totalPackedStockCases: number;
  public totalPackedStockKg: number;
  public disableOnEditPackedStockStatus: Boolean;
  public defaultCrop: Crop = null;

  constructor(public customerSpecService: CustomerSpecService,
    private customerService: CustomerService,
    private cropService: CropService,
    private typeService: TypeService,
    private packedCassesNetKgService: PackedCasesNetKgService,
    private packedStockDetailService: PackedStockDetailService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.cropService.getCrops().subscribe(res => this.crops = res);
    this.typeService.getTypes().subscribe(res => this.types = res);
    this.customerService.getCustomers().subscribe(res => this.customers = res);
    this.packedCassesNetKgService.getAll().subscribe(res => this.packedCasesNetKgList = res);

    this.customerSpec.customer = '';
    this.showAddOrEditModal = false;
    this.showPackedStockModal = false;
    this.addOrEditTitle = 'Add';
    this.addStatus = true;
    this.cropService.getDefaultCrop().subscribe(res => {
      this.defaultCrop = res;
      if (this.defaultCrop == null) {
        this.selectedCrop = (new Date()).getFullYear();
      }
      this.selectedCrop = this.defaultCrop.crop;
      this.selectedType = 'BU';
      this.reloadCustomerSpecList();
    });
    this.disableOnEditPackedStockStatus = false;
  }

  public reloadCustomerSpecList() {
    if (this.selectedCrop == null || this.selectedType == null) {
      return;
    }
    this.customerSpecService.getCustomerSpecByCropAndType(this.selectedCrop, this.selectedType)
      .subscribe(res => this.customerSpecs = res);
  }

  public refresh() {
    this.reloadCustomerSpecList();
  }
  public onCropChange() {
    this.reloadCustomerSpecList();
  }
  public onTypeChange() {
    this.reloadCustomerSpecList();
  }

  public onEditSelected(item: CustomerSpec) {
    this.showAddOrEditModal = true;
    this.addStatus = false;
    this.addOrEditTitle = 'Edit';
    this.customerSpec.id = item.id;
    this.customerSpec.customer = item.customer;
    this.customerSpec.packedGrade = item.packedGrade;
    this.customerSpec.netKg = item.netKg;
    this.customerSpec.manualYield = item.manualYield;
    this.customerSpec.orderCase = item.orderCase;
  }

  public showAddForm() {
    this.showAddOrEditModal = true;
    this.addStatus = true;
    this.addOrEditTitle = 'Add';
    this.customerSpec.crop = this.selectedCrop;
    this.customerSpec.type = this.selectedType;
    this.customerSpec.customer = 'Please select customer';
    this.customerSpec.packedGrade = '';
    this.customerSpec.netKg = 0;
    this.customerSpec.packedCases = 0;
    this.customerSpec.orderCase = 0;
    this.customerSpec.manualYield = 0;
  }

  public add() {
    if (this.customerSpec.crop === null) {
      this.notificationService.showWarning('Please select crop.', 'warning!');
      return;
    }
    if (this.customerSpec.type === '') {
      this.notificationService.showWarning('Please select type.', 'warning!');
      return;
    }
    if (this.customerSpec.customer === '') {
      this.notificationService.showWarning('Please select customer.', 'warning!');
      return;
    }
    if (this.customerSpec.packedGrade === '') {
      this.notificationService.showWarning('Please input packed grade.', 'warning!');
      return;
    }
    if (this.customerSpec.orderCase === null) {
      this.notificationService.showWarning('Please input orderCase.', 'warning!');
      return;
    }
    if (this.customerSpec.netKg === null) {
      this.notificationService.showWarning('Please input netKg.', 'warning!');
      return;
    }
    if (this.customerSpec.manualYield === null) {
      this.notificationService.showWarning('Please input manual yield.', 'warning!');
      return;
    }

    if (this.customerSpec.orderCase <= 0) {
      this.notificationService.showWarning('The order cases should have more than zero.', 'warning!');
      return;
    }
    if (this.customerSpec.netKg <= 0 || this.customerSpec.netKg > 200) {
      this.notificationService.showWarning('A net kgs should have a range between 1-200.', 'warning!');
      return;
    }
    if (this.customerSpec.manualYield <= 0 || this.customerSpec.manualYield > 100) {
      this.notificationService.showWarning('The manual yield should have a range between 1-100.', 'warning!');
      return;
    }

    this.customerSpecService.add(this.customerSpec).subscribe(() => {
        this.reloadCustomerSpecList();
        this.showAddOrEditModal = false;
        this.addStatus = true;
        this.addOrEditTitle = 'Add';
        this.customerSpec.crop = this.selectedCrop;
        this.customerSpec.type = this.selectedType;
        this.customerSpec.customer = 'Please select customer';
        this.customerSpec.packedGrade = '';
        this.customerSpec.netKg = 0;
        this.customerSpec.packedCases = 0;
        this.customerSpec.orderCase = 0;
        this.customerSpec.manualYield = 0;
      });
  }

  public edit() {
    if (confirm('Do you want to edit ' + this.customerSpec.packedGrade + '?')) {

      if (this.customerSpec.crop === null) {
        this.notificationService.showWarning('Please select crop.', 'warning!');
        return;
      }
      if (this.customerSpec.type === '') {
        this.notificationService.showWarning('Please select type.', 'warning!');
        return;
      }
      if (this.customerSpec.customer === '') {
        this.notificationService.showWarning('Please select customer.', 'warning!');
        return;
      }
      if (this.customerSpec.packedGrade === '') {
        this.notificationService.showWarning('Please input packedGrade.', 'warning!');
        return;
      }
      if (this.customerSpec.orderCase === null) {
        this.notificationService.showWarning('Please input orderCase.', 'warning!');
        return;
      }
      if (this.customerSpec.netKg === null) {
        this.notificationService.showWarning('Please input netKg.', 'warning!');
        return;
      }
      if (this.customerSpec.manualYield === null) {
        this.notificationService.showWarning('Please input manual yield.', 'warning!');
        return;
      }

      if (this.customerSpec.orderCase <= 0) {
        this.notificationService.showWarning('The order cases should have more than zero.', 'warning!');
        return;
      }
      if (this.customerSpec.netKg <= 0 || this.customerSpec.netKg > 200) {
        this.notificationService.showWarning('A net kgs should have a range between 1-200.', 'warning!');
        return;
      }
      if (this.customerSpec.manualYield <= 0 || this.customerSpec.manualYield > 100) {
        this.notificationService.showWarning('The manual yield should have a range between 1-100.', 'warning!');
        return;
      }
      this.customerSpecService.edit(this.customerSpec).subscribe(() => {
          this.reloadCustomerSpecList();
          this.showAddOrEditModal = false;
          this.addOrEditTitle = 'Add';
          this.addStatus = true;
        });
    }
  }

  public delete(customerSpec: CustomerSpec) {
    if (confirm('Do you want to delete ' + customerSpec.packedGrade + '?')) {
      this.customerSpecService.delete(customerSpec).subscribe(() => {
          this.reloadCustomerSpecList();
        });
    }
  }

  public onCancel() {
    this.showAddOrEditModal = false;
  }

  public onClickPackedStock(cusSpec: CustomerSpec) {
    this.showPackedStockModal = true;
    this.disableOnEditPackedStockStatus = false;
    this.packedStock.custSpecId = cusSpec.id;
    this.packedStock.packedCasesNetKgID = '0';
    this.packedStock.packedCases = 0;
    this.reloadPackedStockList();
  }

  public addPackedStock() {
    let model: PackedStockDetail = null;
    this.packedStockList.filter((item) => item.packedCasesNetKgID === this.packedStock.packedCasesNetKgID)
      .map((item) => model = item);

    if (model != null) {
      this.notificationService.showError('Dupplicate data in the system.Please delete ' +
        model.netKg + ' kg cases and add new cases again.', 'Error!');
      return;
    }

    this.packedStockDetailService.add(this.packedStock).subscribe(() => {
        this.reloadPackedStockList();
      });

    this.reloadCustomerSpecList();
  }

  public selectedForEditPackedStock(item: PackedStockDetail) {
    this.packedStock.id = item.id;
    this.packedStock.packedCasesNetKgID = item.packedCasesNetKgID;
    this.packedStock.packedCases = item.packedCases;
    this.disableOnEditPackedStockStatus = true;
  }

  public editPackedStock() {
    if (confirm('Are you sure to edit?')) {
      this.packedStockDetailService.edit(this.packedStock).subscribe(() => {
          this.packedStock.id = null;
          this.packedStock.packedCasesNetKgID = '0';
          this.packedStock.packedCases = 0;
          this.packedStock.netKg = 0;
          this.disableOnEditPackedStockStatus = false;
          this.reloadPackedStockList();
        });
    }
    this.reloadCustomerSpecList();
  }

  public deletePackedStock(id: string) {
    if (confirm('Are you sure to delete?')) {
      this.packedStockDetailService.delete(id).subscribe(() => {
          this.reloadPackedStockList();
        });
    }
    this.reloadCustomerSpecList();
  }

  public reloadPackedStockList() {
    this.packedStockDetailService.getByCustomerSpec(this.packedStock.custSpecId)
      .subscribe(res => {
        this.packedStockList = res;
        this.totalPackedStockKg = this.packedStockList.reduce((sum, value) => sum + value.packedCases * value.netKg, 0);
        this.totalPackedStockCases = this.packedStockList.reduce((sum, value) => sum + value.packedCases, 0);
      });
  }
}
