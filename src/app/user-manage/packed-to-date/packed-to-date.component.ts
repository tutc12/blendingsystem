import { TypeService } from '@services/type.service';
import { CropService } from '@services/crop.service';
import { Component, OnInit } from '@angular/core';
import { Crop } from '@models/crop';
import { TobaccoType } from '@models/tobacco-type';
import { PackedToDate } from '@models/packed-to-date';
import { PackedToDateService } from '@services/packed-to-date.service';

@Component({
  selector: 'app-packed-to-date',
  templateUrl: './packed-to-date.component.html',
  styleUrls: ['./packed-to-date.component.css']
})
export class PackedToDateComponent implements OnInit {

  public crops: Crop[];
  public types: TobaccoType[];
  public packedToDateList: PackedToDate[];
  public defaultCrop: Crop = null;
  public selectedCrop: number = null;
  public selectedType: string = null;

  constructor(private cropService: CropService,
    private typeService: TypeService,
    private packedToDateService: PackedToDateService) { }

  ngOnInit() {
    this.cropService.getCrops().subscribe(res => this.crops = res);
    this.typeService.getTypes().subscribe(res => this.types = res);

    this.cropService.getDefaultCrop().subscribe(res => {
      this.defaultCrop = res;
      if (this.defaultCrop == null) {
        this.selectedCrop = (new Date()).getFullYear();
      }
      this.selectedCrop = this.defaultCrop.crop;
      this.selectedType = 'BU';
      this.showPackedToDate();
    });
  }

  showPackedToDate() {
    if (this.selectedCrop == null || this.selectedType == null) {
      return;
    }
    this.packedToDateService.getPackedToDateByCropAndType(this.selectedCrop, this.selectedType)
      .subscribe(res => {
        this.packedToDateList = res;
      });
  }

  save() {
  }
}
