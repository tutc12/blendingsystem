import { TypeService } from '@services/type.service';
import { CropService } from '@services/crop.service';
import { CropSizeService } from '@services/crop-size.service';
import { TobaccoType } from '@models/tobacco-type';
import { Crop } from '@models/crop';
import { CropSize } from '@models/crop-size';
import { Component, OnInit } from '@angular/core';
import { ClrDatagridComparatorInterface } from '@clr/angular';

@Component({
  selector: 'app-crop-size',
  templateUrl: './crop-size.component.html'
})

export class CropSizeComponent implements OnInit {

  public cropSize: CropSize = new CropSize(null, null, null, null, null, null);
  public cropSizes: CropSize[] = [];
  public crops: Crop[] = [];
  public types: TobaccoType[] = [];
  public showModal: Boolean = false;
  public disabledForEdit: Boolean = false;
  public addOrEditTitle: string;
  public cropSizeComparator: CropSizeComparator;

  constructor(private cropSizeService: CropSizeService,
    private cropService: CropService,
    private typeService: TypeService) { }

  ngOnInit() {
    this.addOrEditTitle = 'Add new crop size.';
    this.cropService.getCrops().subscribe(res => this.crops = res);
    this.typeService.getTypes().subscribe(res => this.types = res);
    this.reloadTable();
  }

  public reloadTable() {
    this.cropSizeService.getAll().subscribe(res => {
      this.cropSizes = res;
    });
  }

  public add() {
    this.cropSizeService.add(this.cropSize).subscribe(() => { },
      () => {
        this.reloadTable();
        this.showModal = false;
        this.disabledForEdit = false;
        this.addOrEditTitle = 'Add new crop size.';
        this.cropSize.cropSize = 0;
      });
  }

  public edit() {
    if (confirm('Are you sure to edit ' + this.cropSize.crop + '/' + this.cropSize.type + '?')) {
      this.cropSizeService.edit(this.cropSize).subscribe(() => { },
        () => {
          this.reloadTable();
          this.showModal = false;
          this.disabledForEdit = false;
          this.addOrEditTitle = 'Add new crop size.';
          this.cropSize.cropSize = 0;
        });
    }
  }

  public delete(id: string) {
    if (confirm('Are you sure to delete?' + id)) {
      this.cropSizeService.delete(id).subscribe(() => { },
        () => {
          this.reloadTable();
        });
    }
  }

  public onSelectedForEdit(item: CropSize) {
    this.showModal = true;
    this.disabledForEdit = true;
    this.addOrEditTitle = 'Edit crop size.';
    this.cropSize.id = item.id;
    this.cropSize.crop = item.crop;
    this.cropSize.type = item.type;
    this.cropSize.cropSize = item.cropSize;
  }

  public onClickAdd() {
    this.showModal = true;
    this.addOrEditTitle = 'Add new crop size.';
    this.disabledForEdit = false;
  }

  public onCancel() {
    this.showModal = false;
    this.disabledForEdit = false;
    this.addOrEditTitle = 'Add new crop size.';
    this.cropSize.id = null;
    this.cropSize.crop = null;
    this.cropSize.type = null;
    this.cropSize.cropSize = null;
  }

}

class CropSizeComparator implements ClrDatagridComparatorInterface<CropSize> {
  compare(a: CropSize, b: CropSize) {
    return a.crop - b.crop;
  }
}
