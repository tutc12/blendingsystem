import { CustomerBlendFilter } from '@models/customer-blend-filter';
import { Component, OnInit } from '@angular/core';
import { Crop } from '@models/crop';
import { CropSize } from '@models/crop-size';
import { CustomerBlend } from '@models/customer-blend';
import { CustomerSpec } from '@models/customer-spec';
import { TobaccoType } from '@models/tobacco-type';
import { CustomerSpecFilter } from '@models/customer-spec-filter';
import { CustomerSpecService } from '@services/customer-spec.service';
import { BlendingService } from '@services/blending.service';
import { CropSizeService } from '@services/crop-size.service';
import { TypeService } from '@services/type.service';
import { CropService } from '@services/crop.service';
import { CustomerSpecFilterService } from '@services/customer-spec-filter.service';
import { CustomerBlendFilterService } from '@services/customer-blend-filter.service';
import { NotificationService } from '@services/notification.service';

@Component({
  selector: 'app-blending-plan',
  templateUrl: './blending-plan.component.html',
  styleUrls: ['./blending-plan.component.css']
})
export class BlendingPlanComponent implements OnInit {

  public customerBlends: CustomerBlend[] = [];
  public customerSpecs: CustomerSpec[] = [];
  public crops: Crop[];
  public types: TobaccoType[];
  public selectedCrop: number;
  public selectedType: string;
  public cropSize: number;
  public cropSizeModel: CropSize;
  public accumulativeOfCropThrow = 0;
  public accumulativeOfGreenStock = 0;
  public accumulativeOfExpectedVolumn = 0;
  public accumulativeOfGeenEstimate = 0;
  public accumulativeOfActualBought = 0;
  public accumulativeOfExpectedToBuy = 0;
  public accumulativeOfF = 0;
  public accumulativeOfGreenInputRemaining = 0;
  public loading: Boolean = false;
  public showCustomerSpec: Boolean = true;
  public customerSpecFilters: CustomerSpecFilter[];
  public showCustomerSpecFilter: Boolean = false;
  public customerBlendFilters: CustomerBlendFilter[];
  public showClassifyFilter: Boolean = false;
  public defaultCrop: Crop = null;

  constructor(private blendingService: BlendingService,
    private customerSpecService: CustomerSpecService,
    private cropSizeService: CropSizeService,
    private cropService: CropService,
    private typeService: TypeService,
    private customerSpecFilterService: CustomerSpecFilterService,
    private customerBlendFilterService: CustomerBlendFilterService,
    private notification: NotificationService) {
  }

  public round(number, precision) {
    const factor = Math.pow(10, precision);
    const tempNumber = number * factor;
    const roundedTempNumber = Math.round(tempNumber);
    return roundedTempNumber / factor;
  }

  async ngOnInit() {
    this.crops = await this.cropService.getCrops().toPromise();
    this.types = await this.typeService.getTypes().toPromise();
    const crop: Crop = await this.cropService.getDefaultCrop().toPromise();
    this.selectedCrop = crop.crop;
    this.selectedType = 'BU';

    this.cropSizeModel = await this.cropSizeService.getCropSize(this.selectedCrop, this.selectedType).toPromise();
    this.cropSize = this.cropSizeModel.cropSize;

    this.reloadBlendingPlan();
  }
  public onYieldChange(customerSpec: CustomerSpec) {
    this.calculateGreenInputByBlendPercent(customerSpec);
    this.refreshFooter();
  }
  public onBlendPercentChange(customer: string, packedGrade: string) {
    let customerSpec: CustomerSpec;

    this.customerSpecs.filter((item) => item.customer === customer && item.packedGrade === packedGrade)
      .map((item) => customerSpec = item);

    this.calculateGreenInputByBlendPercent(customerSpec);
    this.refreshFooter();
  }
  public onCropSizeChange() {
    if (this.selectedCrop == null || this.selectedType == null) {
      return;
    }
    this.reloadBlendingPlan();
  }
  public onCropThrowChange() {
    this.accumulativeOfCropThrow = this.customerBlends
      .reduce((sum, value) => sum + value.cropThrow, 0);

    this.accumulativeOfExpectedVolumn = this.customerBlends
      .reduce((sum, value) => sum + ((value.cropThrow / 100) * this.cropSize), 0);
    this.refreshFooter();
  }
  public onCropChange() {
    if (this.selectedCrop == null || this.selectedType == null) {
      return;
    }
    this.reloadBlendingPlan();
  }
  public onTypeChange() {
    if (this.selectedCrop == null || this.selectedType == null) {
      return;
    }
    this.cropSizeService.getCropSize(this.selectedCrop, this.selectedType).subscribe(res => this.cropSize = res.cropSize);
    this.reloadBlendingPlan();
  }
  public reloadPage() {
    location.reload();
  }
  public showCustomerSpecByFilter(cusSpec: CustomerSpec) {
    let model: any;
    this.customerSpecFilters.filter((item) => item.customer === cusSpec.customer &&
      item.packedGrade === cusSpec.packedGrade)
      .map((item) => model = item);

    if (model != null) {
      cusSpec.show = true;
    } else {
      cusSpec.show = false;
    }
  }
  public showOrHideCustomerSpec(cusSpec: CustomerSpec) {
    // show or hide blend percent for each customer blends.
    if (cusSpec.show === true) {
      this.customerSpecFilterService.delete(cusSpec.id).subscribe();
    } else {
      this.customerSpecFilterService.add(cusSpec).subscribe();
    }

    this.customerBlends.forEach((cusBlend) => {
      cusBlend.blends.filter((item) =>
        item.customer === cusSpec.customer && item.packedGrade === cusSpec.packedGrade
      ).map((item) => {
        item.show = !cusSpec.show;
      });
    });
  }
  public showOrHideClassify(cusBlend: CustomerBlend) {
    if (cusBlend.show === true) {
      this.customerBlendFilterService.delete(cusBlend.cropSizeDetailId).subscribe();
    } else {
      this.customerBlendFilterService.add(cusBlend).subscribe();
    }
    this.customerBlends.filter((item) => item.crop === cusBlend.crop
      && item.type === cusBlend.type && item.classify === cusBlend.classify
    ).map((item) => item.show = !cusBlend.show);
  }
  public showClassifyFilterPopup() {
    this.showClassifyFilter = true;
  }
  public getCustomerSpecs() {
    this.customerSpecService.getCustomerSpecByCropAndType(this.selectedCrop, this.selectedType)
      .subscribe(res => this.customerSpecs = res);
  }
  public getCustomerBlends() {
    this.blendingService.getCustomerBlends(this.selectedCrop, this.selectedType)
      .subscribe(res => this.customerBlends = res);
  }
  public getFilter() {
    this.customerBlendFilterService.getByCropAndType(this.selectedCrop, this.selectedType)
      .subscribe(res => this.customerBlendFilters = res);

    this.customerSpecFilterService.getByCropAndType(this.selectedCrop, this.selectedType)
      .subscribe(res => this.customerSpecFilters = res);
  }

  async reloadBlendingPlan() {
    this.loading = true;

    this.customerSpecs = await this.customerSpecService.getCustomerSpecByCropAndType(this.selectedCrop, this.selectedType).toPromise();
    this.customerBlends = await this.blendingService.getCustomerBlendsByCropAndType(this.selectedCrop, this.selectedType).toPromise();
    this.customerSpecFilters = await this.customerSpecFilterService.getByCropAndType(this.selectedCrop, this.selectedType).toPromise();
    this.customerBlendFilters = await this.customerBlendFilterService.getByCropAndType(this.selectedCrop, this.selectedType).toPromise();

    // show or hide customer spec.
    this.customerSpecs.forEach(cusSpec => {
      this.showCustomerSpecByFilter(cusSpec);
      this.calculateGreenInputByBlendPercent(cusSpec);
    });

    // show or hide blend percent for each customer blends.
    this.customerBlends.forEach((cusBlend) => {
      cusBlend.blends.map((item) => item.show = false);
      this.customerSpecFilters.forEach((cusFilter) => {
        cusBlend.blends.filter((item) =>
          item.customer === cusFilter.customer && item.packedGrade === cusFilter.packedGrade
        ).map((item) => item.show = true);
      });

      cusBlend.show = false;
      // Show or hide customer blend for each classify grade.
      this.customerBlendFilters.forEach((filter) => {
        if (cusBlend.crop === filter.crop && cusBlend.type === filter.type && cusBlend.classify === filter.classify) {
          cusBlend.show = true;
        }
      });
    });

    // Refresh footer. *******************************
    // crop throw
    this.accumulativeOfCropThrow = this.customerBlends
      .reduce((sum, value) => sum + value.cropThrow, 0);

    // Expected volumn
    this.accumulativeOfExpectedVolumn = this.customerBlends
      .reduce((sum, value) => sum + ((value.cropThrow / 100) * this.cropSize), 0);

    // Actual bought
    this.accumulativeOfActualBought = this.customerBlends
      .reduce((sum, value) => sum + value.actualBought, 0);

    // Green stock
    this.accumulativeOfGreenStock = this.customerBlends
      .reduce((sum, value) => sum + value.stock, 0);

    // Expected to buy
    this.accumulativeOfExpectedToBuy = this.customerBlends
      .reduce((sum, value) => sum +=
        (((value.cropThrow / 100) * this.cropSize) < value.actualBought ?
          0 : ((value.cropThrow / 100) * this.cropSize) - value.actualBought), 0);

    // Total green balance
    this.accumulativeOfF = this.accumulativeOfGreenStock + this.accumulativeOfExpectedToBuy;

    // Green input require
    this.accumulativeOfGreenInputRemaining = 0;
    this.customerBlends.forEach((cusBlend) => {
      cusBlend.accumulativeRemainningBlending = this.round(cusBlend.blends
        .reduce((sum, value) => sum += value.blendPercent / 100 * value.green, 0), 2);
      this.accumulativeOfGreenInputRemaining += cusBlend.accumulativeRemainningBlending;
    });

    this.refreshFooter();
    this.loading = false;
  }
  public refreshFooter() {
    this.calculateGreenStock();
    this.calculateActualBought();
    this.calculateTobaccoLeftFarmer();
    this.calculateF();
    this.calculateGreenEstimate();
    this.calculateGreenInputByEachOrder();
  }
  public calculateGreenInputByEachOrder() {
    this.accumulativeOfGreenInputRemaining = 0;
    this.customerBlends.forEach((cusBlend) => {
      cusBlend.accumulativeRemainningBlending = this.round(cusBlend.blends
        .reduce((sum, value) => sum += value.blendPercent / 100 * value.green, 0), 2);
      this.accumulativeOfGreenInputRemaining += cusBlend.accumulativeRemainningBlending;
    });
  }
  public calculateGreenInputByBlendPercent(customerSpec: CustomerSpec) {
    let accBlendPercent = 0;
    this.customerBlends.forEach((cusBlend) => {
      cusBlend.blends.forEach((blend) => {
        if (blend.customer === customerSpec.customer && blend.packedGrade === customerSpec.packedGrade) {
          accBlendPercent += blend.blendPercent;
          blend.green =
            (customerSpec.orderCase - customerSpec.packedCases) < 0 ? 0 :
              (((customerSpec.orderCase - customerSpec.packedCases) * customerSpec.netKg)
                - (customerSpec.packedStockKg)) / (customerSpec.manualYield / 100);
        }
      });
    });

    if (accBlendPercent > 100) {
      this.notification.showWarning('A total blend percentages of ' + customerSpec.packedGrade
        + ' is over than 100%', 'warning!');
    }

    this.customerSpecs.filter((item) => item.customer === customerSpec.customer && item.packedGrade === customerSpec.packedGrade)
      .map((item) => item.accumulativeBlenPercent = accBlendPercent);
  }
  public calculateCustomerSpecActualYield() {
    this.customerSpecs.forEach(cusSpec => {
      cusSpec.actualYield = this.round(cusSpec.packedCases <= 0 ? cusSpec.manualYield :
        ((cusSpec.packedCases * cusSpec.netKg) + (cusSpec.packedStockKg))
        / (cusSpec.actualGreen + (cusSpec.packedStockKg)) * 100, 2);

      cusSpec.manualYield = this.round(cusSpec.packedCases <= 0 ? cusSpec.manualYield : cusSpec.actualYield, 2);
      cusSpec.actualGreen = this.round(cusSpec.packedCases <= 0 ? cusSpec.orderCase * cusSpec.netKg / (cusSpec.manualYield / 100)
        : cusSpec.actualGreen, 2);
      // this.calculateGreenInputByBlendPercent(cusSpec);
    });
  }
  public calculateAccumulativeOfExpectedVolumn() {
    this.accumulativeOfExpectedVolumn = this.customerBlends
      .reduce((sum, value) => sum + ((value.cropThrow / 100) * this.cropSize), 0);
  }
  public calculateActualBought() {
    this.accumulativeOfActualBought = this.customerBlends
      .reduce((sum, value) => sum + value.actualBought, 0);
  }
  public calculateGreenStock() {
    this.accumulativeOfGreenStock = this.customerBlends
      .reduce((sum, value) => sum + value.stock, 0);
  }
  public calculateTobaccoLeftFarmer() {
    this.accumulativeOfExpectedToBuy = this.customerBlends
      .reduce((sum, value) => sum +=
        (((value.cropThrow / 100) * this.cropSize) < value.actualBought ?
          0 : ((value.cropThrow / 100) * this.cropSize) - value.actualBought), 0);
  }
  public calculateF() {
    this.accumulativeOfF = this.accumulativeOfGreenStock + this.accumulativeOfExpectedToBuy;
  }
  public calculateGreenEstimate() {
    this.customerSpecs.forEach(cusSpec => {
      cusSpec.accumulativeGreenEstimate = this.customerBlends
        .reduce((sum, value) => sum + value.blends
          .filter((blend) => blend.customer === cusSpec.customer && blend.packedGrade === cusSpec.packedGrade)
          // tslint:disable-next-line:no-shadowed-variable
          .reduce((sum, value) => sum + ((value.blendPercent / 100) * value.green), 0), 0);
    });
  }
  public async save() {
    // edit crop size.
    this.cropSizeModel.cropSize = this.cropSize;
    // const jobA = await this.cropSizeService.edit(this.cropSizeModel).toPromise();

    // // edit customer blend list.
    // const jobB = await this.blendingService.save(this.customerBlends).toPromise();

    // // edit customer spec list.
    // const jobC = await this.customerSpecService.save(this.customerSpecs).toPromise();

    // console.log(jobA);
    // console.log(jobB);
    // console.log(jobC);

    await this.cropSizeService.edit(this.cropSizeModel).toPromise()
      .then(data => { this.notification.showSuccess('Edit crop size successfull.', 'Success!'); });
    await this.blendingService.save(this.customerBlends).toPromise()
      .then(data => { this.notification.showSuccess('Save blending plan successfull.', 'Success!'); });
    await this.customerSpecService.save(this.customerSpecs).toPromise()
      .then(data => { this.notification.showSuccess('customer specification successfull.', 'Success!'); });

    this.reloadBlendingPlan();
  }
  public showCustomerFilterPopup() {
    this.showCustomerSpecFilter = true;
  }
  public hideAllCustomerSpec() {
    this.customerSpecs.filter(item => item.show === true)
      .forEach(cusSpec => {
        this.customerSpecFilterService.delete(cusSpec.id).subscribe();
        cusSpec.show = false;
        this.customerBlends.forEach((cusBlend) => {
          cusBlend.blends.filter(blend => blend.customer === cusSpec.customer && blend.packedGrade === cusSpec.packedGrade)
            .map(item => {
              item.show = cusSpec.show;
            });
        });
      });
  }
  public showAllCustomerSpec() {
    this.customerSpecs.filter(item => item.show === false)
      .forEach(cusSpec => {
        this.customerSpecFilterService.add(cusSpec).subscribe();
        cusSpec.show = true;
        this.customerBlends.forEach((cusBlend) => {
          cusBlend.blends.filter(blend => blend.customer === cusSpec.customer && blend.packedGrade === cusSpec.packedGrade)
            .map(item => {
              item.show = cusSpec.show;
            });
        });
      });
  }
  public showAllCusBlend() {
    this.customerBlends.filter(item => item.show === false)
      .forEach(cusBlend => {
        this.customerBlendFilterService.add(cusBlend).subscribe();
        cusBlend.show = true;
      });
  }
  public hideAllCusBlend() {
    this.customerBlends.filter(item => item.show === true)
      .forEach(cusBlend => {
        this.customerBlendFilterService.delete(cusBlend.cropSizeDetailId).subscribe();
        cusBlend.show = false;
      });
  }
}

