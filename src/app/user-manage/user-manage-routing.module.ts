import { AuthGuard } from './../auth/auth.guard';
import { CropSizeComponent } from './crop-size/crop-size.component';
import { PackedCaseNetKgComponent } from './packed-case-net-kg/packed-case-net-kg.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { BlendingPlanComponent } from './blending-plan/blending-plan.component';
import { PackedToDateComponent } from './packed-to-date/packed-to-date.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CustomerSpecListComponent } from './customer-spec/customer-spec-list/customer-spec-list.component';
import { RoleGuard } from 'app/auth/role.guard';

const userManageRoutes: Routes = [
  {
    path: 'user',
    component: LayoutComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRole: ['Admin', 'User']
    },
    children: [
      { path: '', redirectTo: 'main', pathMatch: 'full' },
      { path: 'main', component: BlendingPlanComponent },
      { path: 'packed-to-date', component: PackedToDateComponent },
      { path: 'customer-spec', component: CustomerSpecListComponent },
      { path: 'packed-case-netkg', component: PackedCaseNetKgComponent },
      { path: 'crop-size', component: CropSizeComponent },
      { path: '**', component: PageNotFoundComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(userManageRoutes)],
  exports: [RouterModule]
})
export class UserManageRoutingModule { }
